import logo from './logo.svg';
import './App.css';

import React from 'react'

import Messages from './components/Messages';
import UserinfoBox from './components/UserinfoBox';

class App extends React.Component{
  constructor(props)
  {
    super(props)
    this.state={
      username:"",
      hasCreatedProfile:false
    }

    this.handleUserNameChange= this.handleUserNameChange.bind(this)
    this.handleUserNameSubmit= this.handleUserNameSubmit.bind(this)

  }

  handleUserNameChange(event) {
    this.setState({username: event.target.value})
  }

  handleUserNameSubmit(event){
    this.setState({hasCreatedProfile: true})
  }

  render(){
    return (
      <div className="App">
        {
        !this.state.hasCreatedProfile?
        <UserinfoBox 
          handleUserNameChange={this.handleUserNameChange}
          handleUserNameSubmit={this.handleUserNameSubmit}
        />
        :
        <Messages username={this.state.username} />
        }
        
      </div>
    );
  }

}


export default App;
