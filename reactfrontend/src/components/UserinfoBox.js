import React from 'react'

class UserinfoBox extends React.Component{
    constructor(props){
        super(props)
        this.state={}
    }
    render(){
        return(
        <div style={{width:"100%",height:"100%",backgroundColor:"#222",position:"absolute"}}>
            <div style={{width:"80%", position:"absolute", top:"50%",left:"50%",transform: "translate(-50%, -50%)"}}>
                <div style={{backgroundColor:"#1C66A6",color:"#FFF",paddingTop:"10px",paddingBottom:"10px"}}> Please put in your name </div>
                <div style={{backgroundColor:"#D9C4B8",paddingTop:"10px",paddingBottom:"10px"}}> 
                    <input type="text" name="name" style={{marginRight:"10px"}} placeholder='Jimmy Jones' onChange={this.props.handleUserNameChange} />
                    <input type="button" value="Start Messaging" onClick={this.props.handleUserNameSubmit}/>
                </div>  
            </div>
        </div>
        )
    }
}

export default UserinfoBox;