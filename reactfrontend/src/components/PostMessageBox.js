import React from 'react'

class PostMessageBox extends React.Component{
    constructor(props){
        super(props)
        this.state={
            messageContent:""
        }
        this.handleAddMessageChange= this.handleAddMessageChange.bind(this)

    }

    handleAddMessageChange(event) {
        this.setState({messageContent: event.target.value})
    }

    render(){
        return(
            <div style={{width:"85%",marginLeft:"auto",marginRight:"auto"}}>
                <div style={{backgroundColor:"#1C66A6",color:"#FFF",paddingTop:"10px",paddingBottom:"10px"}}> Post Your Message Here</div>
                <div style={{backgroundColor:"#D9C4B8",paddingTop:"10px",paddingBottom:"10px"}}> 
                    <textarea name="name" style={{marginRight:"10px",width:"95%"}} 
                    placeholder='Add Your Chat Message Here' onChange={this.handleAddMessageChange} />
                    <br/>
                    <input type="button" style={{marginTop:"10px"}}  value="Send Message" 
                    onClick={async()=>{await this.props.handleAddMessage(this.state.messageContent)}} />
                </div>  
            </div>
        )
    }
}

export default PostMessageBox;