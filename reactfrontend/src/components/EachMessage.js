import React from 'react'
import {RiChatDeleteFill} from 'react-icons/ri'

class EachMessage extends React.Component{
    constructor(props){
        super(props)
        this.state={
            messageID:this.props.messageID,
            posterName:this.props.posterName,
            message:this.props.message
        }
    }

    render(){

        const messageBlockStyle={
            width:"85%",
            fontSize:"14px",
            backgroundColor:"#D9C4B8",
            marginLeft:"auto",
            marginRight:"auto",
            marginBottom:"20px",
            border:"1px solid black",
        }

        const messageTitle={
            fontSize:"15px",
            backgroundColor:"#1C66A6",
            color:'#FFF',
            paddingBottom:"5px",
            paddingTop:"5px",
        }

        const messageContent={
            paddingBottom:"10px",
            paddingTop:"10px",
        }

        return(
            <div style={messageBlockStyle}>
                <div style={messageTitle}>
                    <span>User: {this.state.posterName} Says </span>
                    <span style={{float:"right", marginRight:"10px",cursor:"pointer"}}>
                        {
                         this.props.username===this.state.posterName?
                         <RiChatDeleteFill onClick={async()=>{await this.props.handleRemoveMessage(this.state.messageID)}} />
                         :null
                        }
                    </span>
                </div>
                <div style={messageContent}>{this.state.message} </div>
            </div>
        );
    }


}

export default EachMessage;