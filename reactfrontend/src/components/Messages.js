import React from 'react'
import EachMessage from './EachMessage';
import PostMessageBox from './PostMessageBox';


class Messages extends React.Component{
    constructor(props){
        super(props)
        this.state={
            messageList:[]
        }
        
        this.handleAddMessage= this.handleAddMessage.bind(this)
        this.handleRemoveMessage= this.handleRemoveMessage.bind(this)
    }


    async handleAddMessage(content){
        try {
            await fetch("http://localhost:5000/api/messages",{
                    method: 'POST', 
                    mode: 'cors',
                    cache: 'no-cache', 
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "author_id":this.props.username,
                        "content":content
                    }) 
                }
            )
            //update message list
            let data=await this.getMessagesData()
            this.setState({messageList:data})
            return data
        } catch (e) {
            console.log('Failed to grab message data', e); 
            return false
        }
    }

    async getMessagesData(){
        try {
                let response = await fetch("http://localhost:5000/api/messages",{
                        method: 'GET', 
                        mode: 'cors',
                        cache: 'no-cache', 
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }
                )
            let data=response.json()
            return data
        } catch (e) {
            console.log('Failed to grab message data', e); 
            return false
        }
    }

    async handleRemoveMessage(id){
        try {
            await fetch("http://localhost:5000/api/messages/"+id,{
                    method: 'DELETE', 
                    mode: 'cors',
                    cache: 'no-cache', 
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
            //update message list
            let data= await this.getMessagesData()
            this.setState({messageList:data})
            return data
        } catch (e) {
            console.log('Failed to grab message data', e); 
            return false
        }
        
    }

    async componentDidMount(){
        let messagesData =await this.getMessagesData();
        this.setState({messageList:messagesData})

        setInterval(async()=>{
            let messagesData =await this.getMessagesData();
            this.setState({messageList:messagesData})
        },5000)
    }

    render(){
        const messageListStyle={
           width:"100%",
           marginLeft:"auto",
           marginRight:"auto",
           paddingBottom:"10px",
           paddingTop:"10px",
        }

        const messagesDiv = this.state.messageList.length>0 &&
        this.state.messageList.map((eachMessage) =>
            <EachMessage 
            key={eachMessage.id} 
            posterName={eachMessage.author_id}
            username={this.props.username}
            message={eachMessage.content} 
            messageID={eachMessage.id}
            handleRemoveMessage={this.handleRemoveMessage}
            />
        );

        console.log(this.state.messageList)
        return(
            <div style={messageListStyle}>
                <h3>You are: {this.props.username}</h3>
                {this.state.messageList.length>0?messagesDiv:<h3>No Messages</h3>}
                {<PostMessageBox handleAddMessage={this.handleAddMessage}/>}
            </div>
        )
    }
}

export default Messages;